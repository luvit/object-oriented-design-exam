# Welcome to Object Oriented Design Exam!

## INSTRUCTIONS:

The idea here is to solve this exercise the best way you can think of.  
You can use whatever language you want in order to solve it, however it must be an object oriented language such as python, ruby, swift, kotlin, java or c#.

Source Code Management:  
Please create a new public repo and send us the link as soon as you finish the exercise.  
Please try to commit little chunks of code, do not commit all at once, so we can track your progress and your commit style.  

The link to the repo must be sent to **ro@plataforma.io & kev@luv.it**  
Please attach in the email your CV so we can understand your background.  

## THE EXERCISE:

This exercise consists of creating a method that receives an order and that order has an arrangement of products.  
Such method has to answer if the order is correct or not.  
In case it does not pass the validations, it would have to return an error message indicating which of the validations do not pass **(all of the validations, not just the first one)**.    
To solve the exercise you can create all the objects you think are necessary but **Order** and **Product** must follow this structure:  

```
Order {
  var id: String
  var products: [Product]
}
```
```
Product {
  var id: String
  var name: String
  var price: Double?
  var available: Bool
}
```


### The validations that must be applied are the followings:


* All products in the order must be available = true.

* The product array can not be empty.

* All products must have a value in assigned price, if it has no value, then that product wont pass the validation.

* If the name of a product is "unknown" it should not pass the validation.

*There is more than one way to solve this exercise, take it easy and ask all the questions you think are necessary, you can do it :).*

Good Luck!
